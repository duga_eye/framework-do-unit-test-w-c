#include <stdio.h>
#include "minunit.h"

#define BUFFER_SIZE 0x1000

int tests_run = 0;
unsigned int pos = 0;
unsigned int limit = BUFFER_SIZE;

char nbuffer[BUFFER_SIZE];
char *buffer = (char *)nbuffer;
char ntemp[BUFFER_SIZE];
char *temp = (char *)ntemp;
unsigned int temp_limit = BUFFER_SIZE;
int tests_succeeded = 0;

int mu_strlen(const char *s) {
    int count = 0;
    char *r = (char *)s;
    while (*r++) count++;
    return count;
}

void *mu_memcpy(void *dest, const void *src, unsigned int size) {
    int i; char *d = (char *)dest; const char *s = (char *)src;
    for (i = 0; i < size; i++) d[i] = s[i];
    return dest;
}

int max(int a, int b) {
    return a>b ? a : b;
}

static char *test1() {
    mu_assert("niepoprawny wynik1 dla test1", max(2,3)==3);
    return 0;
}

static char *test2() {
    mu_assert("niepoprawny wynik1 dla test2", max(2,3)==2);
    return 0;
}

static char *test3() {
    mu_assert("niepoprawny wynik1 dla test3", max(2,3)==4);
    return 0;
}


char *all_tests() {
    mu_run_test(test1);
    mu_run_test(test2);
    mu_run_test(test3);
    return 0;
}

int main() {
    char *error = all_tests();
    if (error != 0) {
        printf("%s\n", error);
    } else if (tests_succeeded != tests_run) {
        printf("%s\n", buffer);
    } else {
        printf("ALL TESTS PASSED\n");
    }
    printf("Tests run: %d, succeeded: %d\n", tests_run, tests_succeeded);
    return tests_succeeded != tests_run;
}
