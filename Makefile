.PHONY: clean test

all: test.c.o
	gcc -g -o test $<
%.c.o: %.c
	gcc -c -g -o $@ $<

clean:
	rm -f *.o test

test: all
	./test
